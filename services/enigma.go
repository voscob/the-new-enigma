package enigma

func Encode(text string) string {
	var vowels = map[rune]rune{
		'a': '1',
		'e': '2',
		'i': '3',
		'o': '4',
		'u': '5',
	}

	nw := make([]rune, len(text))
	for i, v := range text {
		if k, ok := vowels[v]; ok {
			nw[i] = k
		} else {
			nw[i] = v
		}
	}

	return string(nw)
}

func Decode(text string) string {
	var digits = map[rune]rune{
		'1': 'a',
		'2': 'e',
		'3': 'i',
		'4': 'o',
		'5': 'u',
	}

	nw := make([]rune, len(text))
	for i, v := range text {
		if k, ok := digits[v]; ok {
			nw[i] = k
		} else {
			nw[i] = v
		}
	}

	return string(nw)
}