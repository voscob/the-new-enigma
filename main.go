package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	enigma "gitlab.com/voscob/the-new-enigma/services"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Select an option:\n1) encode text;\n2) decode text")
	option, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Ooops. Something went wrong :(")
	}

	option = strings.TrimSuffix(option, "\n")
	if n, err := strconv.Atoi(option); err == nil {
		if n == 1 || n == 2 {
			fmt.Print("Enter text: ")
			text, err := reader.ReadString('\n')
			if err != nil {
				fmt.Println("Ooops. Something went wrong :(")
			}

			switch n {
			case 1:
				fmt.Println("Encoded:", enigma.Encode(text))
			case 2:
				fmt.Println("Decoded:", enigma.Decode(text))
			}
		}
	} else {
		fmt.Println(option, "is not an integer.")
	}
}
